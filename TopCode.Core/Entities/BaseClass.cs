﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TopCode.Core.Entities
{
    public class BaseClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        [Required]
        public long created_by { get; set; }
        [Required]
        public DateTime created_on { get; set; }

        public long updated_by { get; set; }

        public DateTime updated_on { get; set; }
    }

    public class HalfBaseClass
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        [Required]
        public long created_by { get; set; }
        [Required]
        public DateTime created_on { get; set; }

    }
}
