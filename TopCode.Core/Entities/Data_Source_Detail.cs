﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TopCode.Core.Entities
{
    public class Data_Source_Detail : IdentityClass
    {
        
        [Required]
        [MaxLength(255)]
        public string extraction_type { get; set; }
        public string field_mapping { get; set; }
        [MaxLength(125)]
        public string target_table { get; set; }
        [MaxLength(100)]
        public string cron_expression { get; set; }
        [MaxLength(100)]
        public string name { get; set; }
        public string source_detail { get; set; }
        [Required]
        [MaxLength(255)]
        public string persist_mechanism { get; set; }
        public DateTime last_run_on { get; set; }

        public string status { get; set; }
    }
    
}
