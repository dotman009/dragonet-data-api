﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TopCode.Core.Entities
{
    public class Data_Source_Table : BaseClass
    {
        [MaxLength(100)]
        public string name { get; set; }
        [MaxLength(125)]
        public string data_source_table { get; set; }
        [Required]
        public bool canEditData { get; set; }
        public long user_id { get; set; }
        public virtual User User { get; set; }
    }
}
