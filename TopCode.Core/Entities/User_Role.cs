﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TopCode.Core.Entities
{
    public class User_Role : BaseClass
    {
        public long user_id { get; set; }
        public virtual User User { get; set; }

        public string Role { get; set; }

    }
}
