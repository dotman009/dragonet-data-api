﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TopCode.Core.Entities
{
    public class User : BaseClass
    {
        
        [Required]
        [MaxLength(255)]
        public string full_name { get; set; }
        [Required]
        [MaxLength(256)]
        public string email { get; set; }
        [Required]
        [MaxLength(255)]
        public string status { get; set; }
        
    }
}
