﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TopCode.Core.Entities
{
    public class Kri_Metric : BaseClass
    {
        public long security_area_id { get; set; }
        public virtual Security_Area Security_Area { get; set; }

        [MaxLength(200)]
        public string name { get; set; }

        [MaxLength(255)]
        public string data_update_mechanism { get; set; }
        [MaxLength(255)]
        public string unit { get; set; }

        public string formula { get; set; }
        [MaxLength(100)]
        public string cron_expression { get; set; }
        [MaxLength(1000)]
        public string risk_color_formula { get; set; }

        public long user_id { get; set; }
        public virtual User User { get; set; }
    }
}
