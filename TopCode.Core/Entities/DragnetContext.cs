﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace TopCode.Core.Entities
{
    public class DragnetContext : DbContext
    {
        public DragnetContext(DbContextOptions<DragnetContext> options): base(options) 
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<User_Role> User_Roles { get; set; }
        public DbSet<Data_Source_Detail> Data_Source_Details { get; set; }
        public DbSet<Data_Source_Table> Data_Source_Tables { get; set; }
        public DbSet<Kri_Comment> Kri_Comment { get; set; }
        public DbSet<Kri_Metric> kri_Metrics { get; set; }
        public DbSet<Kri_Value> kri_Values { get; set; }
        public DbSet<Security_Area> Security_Areas { get; set; }
        public DbSet<Audit_Log> Audit_Logs { get; set; }
        public DbSet<Error_Log> Error_Logs { get; set; }
        public DbSet<Ds_Xxx> Ds_Xxxs { get; set; }
    }
}
