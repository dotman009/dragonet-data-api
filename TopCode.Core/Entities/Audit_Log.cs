﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TopCode.Core.Entities
{
    public class Audit_Log : HalfBaseClass
    {
        [MaxLength(500)]
        public string message { get; set; }
    }
}
