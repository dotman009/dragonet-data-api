﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TopCode.Core.Entities
{
    public class Kri_Comment : HalfBaseClass
    {
        public long kri_value_id { get; set; }
        public virtual Kri_Value Kri_Value { get; set; }

        [Required]
        [MaxLength(500)]
        public string comment { get; set; }

        public long user_id { get; set; }
        public virtual User User { get; set; }


    }
}
