﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TopCode.Core.Entities
{
    public class Error_Log: IdentityClass
    {
        [Required]
        [MaxLength(300)]
        public string message { get; set; }
        [Required]
        [MaxLength(125)]
        public string table_name { get; set; }
        public string details { get; set; }
        public DateTime created_on { get; set; }
    }
}
