﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TopCode.Core.Entities
{
    public class Kri_Value : BaseClass
    {
        public long kri_metric_id { get; set; }
        public virtual Kri_Metric Kri_Metric { get; set; }

        [Required]
        [MaxLength(255)]
        public string business_unit { get; set; }

        [Required]
        public int revision { get; set; }
        [Required]
        public decimal value { get; set; }

        public long user_id { get; set; }
        public virtual User User { get; set; }
    }
}
