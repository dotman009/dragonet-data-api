﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TopCode.Core.Entities
{
    public class Security_Area : BaseClass
    {
        [MaxLength(100)]
        public string name { get; set; }
        public long user_id { get; set; }
        public virtual User User { get; set; }
    }
}
