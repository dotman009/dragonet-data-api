﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TopCode.Core.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Audit_Logs",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    message = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Audit_Logs", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Data_Source_Details",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    extraction_type = table.Column<string>(maxLength: 255, nullable: false),
                    field_mapping = table.Column<string>(nullable: true),
                    target_table = table.Column<string>(maxLength: 125, nullable: true),
                    cron_expression = table.Column<string>(maxLength: 100, nullable: true),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    source_detail = table.Column<string>(nullable: true),
                    persist_mechanism = table.Column<string>(maxLength: 255, nullable: false),
                    last_run_on = table.Column<DateTime>(nullable: false),
                    status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Data_Source_Details", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Ds_Xxxs",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    updated_by = table.Column<long>(nullable: false),
                    updated_on = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ds_Xxxs", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Error_Logs",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    message = table.Column<string>(maxLength: 300, nullable: false),
                    table_name = table.Column<string>(maxLength: 125, nullable: false),
                    details = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Error_Logs", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    updated_by = table.Column<long>(nullable: false),
                    updated_on = table.Column<DateTime>(nullable: false),
                    full_name = table.Column<string>(maxLength: 255, nullable: false),
                    email = table.Column<string>(maxLength: 256, nullable: false),
                    status = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Data_Source_Tables",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    updated_by = table.Column<long>(nullable: false),
                    updated_on = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    data_source_table = table.Column<string>(maxLength: 125, nullable: true),
                    canEditData = table.Column<bool>(nullable: false),
                    user_id = table.Column<long>(nullable: false),
                    Userid = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Data_Source_Tables", x => x.id);
                    table.ForeignKey(
                        name: "FK_Data_Source_Tables_Users_Userid",
                        column: x => x.Userid,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Security_Areas",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    updated_by = table.Column<long>(nullable: false),
                    updated_on = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    user_id = table.Column<long>(nullable: false),
                    Userid = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Security_Areas", x => x.id);
                    table.ForeignKey(
                        name: "FK_Security_Areas_Users_Userid",
                        column: x => x.Userid,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "User_Roles",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    updated_by = table.Column<long>(nullable: false),
                    updated_on = table.Column<DateTime>(nullable: false),
                    user_id = table.Column<long>(nullable: false),
                    Userid = table.Column<long>(nullable: true),
                    Role = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Roles", x => x.id);
                    table.ForeignKey(
                        name: "FK_User_Roles_Users_Userid",
                        column: x => x.Userid,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "kri_Metrics",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    updated_by = table.Column<long>(nullable: false),
                    updated_on = table.Column<DateTime>(nullable: false),
                    security_area_id = table.Column<long>(nullable: false),
                    Security_Areaid = table.Column<long>(nullable: true),
                    name = table.Column<string>(maxLength: 200, nullable: true),
                    data_update_mechanism = table.Column<string>(maxLength: 255, nullable: true),
                    unit = table.Column<string>(maxLength: 255, nullable: true),
                    formula = table.Column<string>(nullable: true),
                    cron_expression = table.Column<string>(maxLength: 100, nullable: true),
                    risk_color_formula = table.Column<string>(maxLength: 1000, nullable: true),
                    user_id = table.Column<long>(nullable: false),
                    Userid = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_kri_Metrics", x => x.id);
                    table.ForeignKey(
                        name: "FK_kri_Metrics_Security_Areas_Security_Areaid",
                        column: x => x.Security_Areaid,
                        principalTable: "Security_Areas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_kri_Metrics_Users_Userid",
                        column: x => x.Userid,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "kri_Values",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    updated_by = table.Column<long>(nullable: false),
                    updated_on = table.Column<DateTime>(nullable: false),
                    kri_metric_id = table.Column<long>(nullable: false),
                    Kri_Metricid = table.Column<long>(nullable: true),
                    business_unit = table.Column<string>(maxLength: 255, nullable: false),
                    revision = table.Column<int>(nullable: false),
                    value = table.Column<decimal>(nullable: false),
                    user_id = table.Column<long>(nullable: false),
                    Userid = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_kri_Values", x => x.id);
                    table.ForeignKey(
                        name: "FK_kri_Values_kri_Metrics_Kri_Metricid",
                        column: x => x.Kri_Metricid,
                        principalTable: "kri_Metrics",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_kri_Values_Users_Userid",
                        column: x => x.Userid,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Kri_Comment",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    kri_value_id = table.Column<long>(nullable: false),
                    Kri_Valueid = table.Column<long>(nullable: true),
                    comment = table.Column<string>(maxLength: 500, nullable: false),
                    user_id = table.Column<long>(nullable: false),
                    Userid = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kri_Comment", x => x.id);
                    table.ForeignKey(
                        name: "FK_Kri_Comment_kri_Values_Kri_Valueid",
                        column: x => x.Kri_Valueid,
                        principalTable: "kri_Values",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Kri_Comment_Users_Userid",
                        column: x => x.Userid,
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Data_Source_Tables_Userid",
                table: "Data_Source_Tables",
                column: "Userid");

            migrationBuilder.CreateIndex(
                name: "IX_Kri_Comment_Kri_Valueid",
                table: "Kri_Comment",
                column: "Kri_Valueid");

            migrationBuilder.CreateIndex(
                name: "IX_Kri_Comment_Userid",
                table: "Kri_Comment",
                column: "Userid");

            migrationBuilder.CreateIndex(
                name: "IX_kri_Metrics_Security_Areaid",
                table: "kri_Metrics",
                column: "Security_Areaid");

            migrationBuilder.CreateIndex(
                name: "IX_kri_Metrics_Userid",
                table: "kri_Metrics",
                column: "Userid");

            migrationBuilder.CreateIndex(
                name: "IX_kri_Values_Kri_Metricid",
                table: "kri_Values",
                column: "Kri_Metricid");

            migrationBuilder.CreateIndex(
                name: "IX_kri_Values_Userid",
                table: "kri_Values",
                column: "Userid");

            migrationBuilder.CreateIndex(
                name: "IX_Security_Areas_Userid",
                table: "Security_Areas",
                column: "Userid");

            migrationBuilder.CreateIndex(
                name: "IX_User_Roles_Userid",
                table: "User_Roles",
                column: "Userid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Audit_Logs");

            migrationBuilder.DropTable(
                name: "Data_Source_Details");

            migrationBuilder.DropTable(
                name: "Data_Source_Tables");

            migrationBuilder.DropTable(
                name: "Ds_Xxxs");

            migrationBuilder.DropTable(
                name: "Error_Logs");

            migrationBuilder.DropTable(
                name: "Kri_Comment");

            migrationBuilder.DropTable(
                name: "User_Roles");

            migrationBuilder.DropTable(
                name: "kri_Values");

            migrationBuilder.DropTable(
                name: "kri_Metrics");

            migrationBuilder.DropTable(
                name: "Security_Areas");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
